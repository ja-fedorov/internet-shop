package model;

import lombok.Data;

@Data
public class Cart {
    private long id;
    private User user;
    private Order order;
}
