package model;

import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class  Order {
    private long id;
    private User user;
    private List<Product> products;
    private Status status;
    private Date createdDate;
    private Date updatedDate;
    private String address;
    public enum Status {
        NEW, PAID, DELIVERED, CANCELED, RETURNED
    }
}
