package model;

import lombok.Data;

@Data
public  class User {
    private long id;

    private Role role;

    private String name;
    private String surname;
    private String password;
    private String phone;
    private String email;

    public User() {}

    public User(long id, Role role, String name, String surname, String password, String phone, String email) {
        this.id = id;
        this.role = role;
        this.name = name;
        this.surname = surname;
        this.password = password;
        this.phone = phone;
        this.email = email;
    }
}
