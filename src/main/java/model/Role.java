package model;

import lombok.Data;

@Data
public class Role {
    private long id;
    private RoleName roleName;

    public Role(RoleName roleName) {
        this.roleName = roleName;
    }

    public enum RoleName {
        ADMIN, USER, GUEST, MANAGER
    }
}
