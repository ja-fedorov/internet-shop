package model;

import lombok.Data;

@Data
public class Product {
    private long id;
    private String name;
    private double price;
    private int quantity;
    private String description;

    public Product () {}

    public Product(long id, String name, double price, int quantity, String description) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.description = description;
    }
}
